//
//  LineModel.swift
//  Line
//
//  Created by 謝欣岑 on 2018/5/12.
//  Copyright © 2018年 謝欣岑. All rights reserved.
//

import Foundation

struct LineModel {
    let id: Int
    var name: String
    var message: String
    var image: String
    func rowData() -> [String: String] {
        return ["name": name, "message": message, "image": image]
    }
}
