//
//  MyCellTableViewCell.swift
//  Line
//
//  Created by 謝欣岑 on 2018/5/12.
//  Copyright © 2018年 謝欣岑. All rights reserved.
//

import UIKit

class MyCellTableViewCell: UITableViewCell {
    
//  -------------------------------------------------
    @IBOutlet weak var cellPhoto: UIImageView!
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellMessage: UILabel!
//  -------------------------------------------------
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
