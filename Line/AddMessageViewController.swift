//
//  AddMessageViewController.swift
//  Line
//
//  Created by 謝欣岑 on 2018/5/12.
//  Copyright © 2018年 謝欣岑. All rights reserved.
//

import UIKit

var handleImage:HandleImage?
class AddMessageViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
//  ------------------------------------------------------
    @IBOutlet weak var add_image: UIImageView!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var messageText: UITextField!
    @IBAction func selectPhotoButton(_ sender: UIButton) {
        let imagePickerVC = UIImagePickerController()
        //  設定相片的來源為行動裝置內的相本
        imagePickerVC.sourceType = .photoLibrary
        imagePickerVC.delegate = self
        
        //  設定顯示模式為popover
        imagePickerVC.modalPresentationStyle = .popover
        let popover = imagePickerVC.popoverPresentationController
        //  設定popover視窗與哪一個view元件有關連
        popover?.sourceView = sender
        
        //  以下兩行處理popover的箭頭位置
        popover?.sourceRect = sender.bounds
        popover?.permittedArrowDirections = .any
        
        show(imagePickerVC, sender: self)
    }
    @IBAction func insertMessageButton(_ sender: UIButton) {
        if let mydb = db {
            models.removeAll()
            if let image = add_image.image{
                // insert
                let _ = mydb.insert("line", rowInfo: ["name": "'\(nameText.text!)'", "message": "'\(messageText.text!)'", "image":"'\(handleImage?.imageToSting(add_image.image!))'"])
                print("照片在這\(handleImage?.imageToSting(add_image.image!))")
            }else{
                // insert
                let _ = mydb.insert("line", rowInfo: ["name": "'\(nameText.text!)'", "message": "'\(messageText.text!)'", "image":"'domo.jpg'"])
            }
            
            // select
            let statement = mydb.fetch("line", cond: "1 == 1", order: nil)
            while sqlite3_step(statement) == SQLITE_ROW{
                let id = sqlite3_column_int(statement, 0)
                let name = String(cString: sqlite3_column_text(statement, 1))
                let message = String(cString: sqlite3_column_text(statement, 2))
                let image = String(cString: sqlite3_column_text(statement, 3))
                let model = LineModel(id: Int(id), name: name, message: message, image: image)
                models.append(model)
            }
            sqlite3_finalize(statement)
        }
        self.navigationController?.popViewController(animated: true)
    }
//  ------------------------------------------------------
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        add_image.image = image
        dismiss(animated: true, completion: nil)
    }
    
    ///  圖片轉base64字串
    func imageToSting(_ image:UIImage)->String{
        let imageData: Data = UIImageJPEGRepresentation(image, 0.7)!
        let dataString = imageData.base64EncodedString()
        
        return dataString
    }
//  ------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//  ------------------------------------------------------

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
