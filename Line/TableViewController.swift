//
//  TableViewController.swift
//  Line
//
//  Created by 謝欣岑 on 2018/5/12.
//  Copyright © 2018年 謝欣岑. All rights reserved.
//

import UIKit

var db :SQLiteConnect?
var models = [LineModel]()

class TableViewController: UITableViewController {
    
//  -------------------------------------------------
    @IBAction func addButton(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "TableToAdd", sender: nil)
    }
//  -------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        // 資料庫檔案的路徑
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let sqlitePath = urls[urls.count-1].absoluteString + "db.sqlite3"
        
        // SQLite 資料庫
        db = SQLiteConnect(path: sqlitePath)
        
        if let mydb = db {
            // create table
            let _ = mydb.createTable("line", columnsInfo: [
                "id integer primary key autoincrement",
                "name text",
                "message text",
                "image text"])
            
            // insert
//            let _ = mydb.insert("line", rowInfo: ["name": "'佳瑋'", "message": "'是狗狗'", "image":"'domo.jpg'"])
            
            // select
            let statement = mydb.fetch("line", cond: "1 == 1", order: nil)
            while sqlite3_step(statement) == SQLITE_ROW{
                let id = sqlite3_column_int(statement, 0)
                let name = String(cString: sqlite3_column_text(statement, 1))
                let message = String(cString: sqlite3_column_text(statement, 2))
                let image = String(cString: sqlite3_column_text(statement, 3))
                let model = LineModel(id: Int(id), name: name, message: message, image: image)
                models.append(model)
            }
            sqlite3_finalize(statement)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return models.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyCellTableViewCell
        cell.cellName.text = models[indexPath.row].name
        cell.cellMessage.text = models[indexPath.row].message
        if models[indexPath.row].image == "domo.jpg"{
            cell.cellPhoto.image = UIImage(named: models[indexPath.row].image)
        }else{
            cell.cellPhoto.image = handleImage?.tryParseImage(models[indexPath.row].image)
        }
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        let id = String(models[indexPath.row].id)
        
        /// 先刪除db的資料
        if let mydb = db {
            // delete
            let _ = mydb.delete("line", cond: "id = \(id)")
        }
        
        /// 在刪除models的資料
        models.remove(at: indexPath.row)
        
        /// 在清除table的資料
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

/*
 // update
 let _ = mydb.update("students", cond: "id = 1", rowInfo: ["name":"'小強'","height":"176.8"])
 

 */
