//
//  HandleImage.swift
//  GoldenBrotherApp
//
//  Created by 謝佳瑋 on 2016/12/13.
//  Copyright © 2016年 謝佳瑋. All rights reserved.
//

import Foundation
import UIKit
///  處理圖片字串轉換
class HandleImage{
    
    init(){}
    /// base64字串轉圖片
    private func parseImage(_ imageString:String)throws ->UIImage{
        guard imageString != "" else{
            throw parseImageError.imageStringEmpty
        }
        let imageData = Data(base64Encoded: imageString, options: .ignoreUnknownCharacters)
        let img = UIImage(data: imageData!)
        return img! as UIImage
        
    }
    ///  定義錯誤型態
    enum parseImageError :Error {
        case imageStringEmpty
    }
    /// 嘗試轉換圖片
    func tryParseImage(_ imageString:String)->UIImage{
        do{
            let image = try self.parseImage(imageString)
            return image
        }catch parseImageError.imageStringEmpty{
            return UIImage(named: "photo")!
        }catch{
            return UIImage(named: "photo")!
        }
    }
    
    
    //------------------------上傳圖片相關function-------------------------------
    ///  圖片轉base64字串
    func imageToSting(_ image:UIImage)->String{
        let imageData: Data = UIImageJPEGRepresentation(image, 0.7)!
        let dataString = imageData.base64EncodedString()
        
        return dataString
    }
    ///  修改圖片尺寸
    func imageRsize(_ image : UIImage)->UIImage{
        let newSize:CGSize = CGSize(width: 58,height: 58)
        let rect = CGRect(x: 0,y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        
        // image is a variable of type UIImage
        image.draw(in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func imageRsize(_ image : UIImage,w:Int,h:Int)->UIImage{
        let newSize:CGSize = CGSize(width: w,height: h)
        let rect = CGRect(x: 0,y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        
        // image is a variable of type UIImage
        image.draw(in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
