
SQLite相關資料
https://itisjoe.gitbooks.io/swiftgo/content/database/sqlite.html

1.加入SQLite
General > Linked Frameworks and Libraries，並按下加號 + ，選擇libsqlite3.tbd

2.新增header檔案
iOS > Source > Header File
檔案命名為 BridgeHeader.h (.h就是一個header檔案)，記得勾選Targets
在檔案加入 #include "sqlite3.h" 這一行程式

3.與Objective-C連結
TARGETS > (檔名) > Build Settings > Objective-C Bridging Header
填入 header 檔案的路徑與名稱
